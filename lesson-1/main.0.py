import numpy as np
import cv2 as cv

plate_cascade = cv.CascadeClassifier('../cascades/haarcascade_russian_plate_number.xml')

def detect_plate(image_filename):
    image = cv.imread(image_filename)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    plates = plate_cascade.detectMultiScale(gray, 1.1, 5)
    for (x,y,w,h) in plates:
        cv.rectangle(image, (x,y), (x+w, y+h), (255,0,0), 2)
    return image

cv.imshow("show", detect_plate('../images/lesson-1-raw/image_0.png'))
cv.waitKey(0)
cv.destroyAllWindows()