import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import os
import math

images_dir = '../images/lesson-1-result/'

def threshold_image(image):
    image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    image = cv.adaptiveThreshold(image,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,11,1)
    return image

orig_image = cv.imread('{}res-4.jpg'.format(images_dir))
thr_image = threshold_image(orig_image)

_,ax = plt.subplots(2,1)
ax[0].imshow(orig_image)
ax[1].imshow(thr_image, cmap='gray')

plt.show()