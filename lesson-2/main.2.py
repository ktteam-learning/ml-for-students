import numpy as np
import cv2 as cv
import os
import math

images_dir = '../images/lesson-1-result/'
save_dir = '../images/lesson-2-result/'
shape_size = 64
rect_side_min = 12
rect_side_max = 30

def threshold_image(image):
    image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    image = cv.adaptiveThreshold(image,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,11,1)
    return image

def find_contours(image):
    _, contours, _ = cv.findContours(image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    contours = sort_contours(contours)

    contours_poly = [None]*len(contours)
    rects = []

    for i, contour in enumerate(contours):
        contours_poly[i] = cv.approxPolyDP(contour, 3, True)
        (x,y,w,h) = cv.boundingRect(contours_poly[i])
        if (w > rect_side_min and  w < rect_side_max) and (h > rect_side_min and h < rect_side_max):
            rects.append((x,y,w,h))
    
    return rects

def split_contours(image):
    rects = find_contours(image)
    result = []
    for (x,y,w,h) in rects:
        new_image = np.zeros((shape_size,shape_size))
        new_image[:] = 255
        x_coord = int(shape_size/2 - w/2)
        y_coord  = int(shape_size/2 - h/2)
        new_image[y_coord:y_coord+h,x_coord:x_coord+w] = image[y:y+h,x:x+w]
        result.append(new_image)    
    return result

def sort_contours(contours):
    sorted_ctrs = sorted(contours, key=lambda ctr: cv.boundingRect(ctr)[0])
    return sorted_ctrs

files = os.listdir(images_dir)
for file in files:
    orig_image = cv.imread('{}{}'.format(images_dir,file))
    thr_image = threshold_image(orig_image)
    symbols = split_contours(thr_image)
    for i in range(len(symbols)):
        result_dir = '{}/{}/'.format(save_dir, os.path.splitext(file)[0])
        result_file = '{}symbol_{}.jpg'.format(result_dir, i)
        os.makedirs(result_dir, exist_ok=True)
        cv.imwrite(result_file, symbols[i])
 