import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import os
import math

images_dir = '../images/lesson-1-result/'
save_dir = '../images/lesson-2-result/'
rect_side_min = 12
rect_side_max = 30

def threshold_image(image):
    image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    image = cv.adaptiveThreshold(image,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,11,1)
    return image

def find_contours(image):
    _, contours, _ = cv.findContours(image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    contours = sort_contours(contours)

    contours_poly = [None]*len(contours)
    rects = []

    for i, contour in enumerate(contours):
        contours_poly[i] = cv.approxPolyDP(contour, 3, True)
        (x,y,w,h) = cv.boundingRect(contours_poly[i])
        if (w > rect_side_min and  w < rect_side_max) and (h > rect_side_min and h < rect_side_max):
            rects.append((x,y,w,h))
    
    return rects

def draw_rects(image):
    res_image = cv.cvtColor(image, cv.COLOR_GRAY2BGR)
    rects = find_contours(image)
    for (x,y,w,h) in rects:
        cv.rectangle(res_image, (x, y), (x+w, y+h), (255,0,0), 1)
    return res_image

def sort_contours(contours):
    sorted_ctrs = sorted(contours, key=lambda ctr: cv.boundingRect(ctr)[0])
    return sorted_ctrs

orig_image = cv.imread('{}res_4.jpg'.format(images_dir))
thr_image = threshold_image(orig_image)
cnt_image = draw_rects(thr_image)

_,ax = plt.subplots(3,1)
ax[0].imshow(orig_image)
ax[1].imshow(thr_image, cmap='gray')
ax[2].imshow(cnt_image, cmap='gray')

plt.show()